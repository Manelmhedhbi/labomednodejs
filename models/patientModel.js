const mongoose=require('mongoose')
const user = require('./userModel')

const patientSchema = user.discriminator("patient",new mongoose.Schema({

    medecin:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'medecin'
    },
    dossier:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'dossier'
    }],
   
    rdv:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'rdv'
    }],
    analyse:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'analyse'
    }]
 
})
)
module.exports=patientSchema