const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const bcrypt=require('bcrypt');
var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};
var regex = function(phone){
    var rg= /^\+?(216)([0-9]{8})$/ ;
         return rg.test(phone)
};
var passw=function(password){
    var pw=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,10}$/;
    return pw.test(password)
}

const userSchema= new Schema({
    nom:{
        type: String,
        required:true // champ obligatoir
    },
    prenom:{
        type: String,
        required:true
    },
    email:{
        type: String,
        required:true,
        validate:[validateEmail],
        unique:true,
    },
    password:{
        type: String,
        required:true,
        validate:[passw]
    },
    image:{
        type:String,
        required:true
    },
    phone:{
        type: Number,
        required:true,
        //minlength:11,
        //maxlenght:11,
        validate:[regex]
    },
    adress:{
        type: String,
        required:true,
    },
    genre:{
        type: String,
        required:true,
    },
    role:{
        type: String,
        required:true,
    },
    resetLink:{
        type:String,
        default:""
    }
    
}) 
//.pre : methode de sauvegard
userSchema.pre('save', function (next) {
    this.password=bcrypt.hashSync(this.password, 10) 
    next()
});
module.exports=mongoose.model('user',userSchema)  //'user': nom de classe dans bd