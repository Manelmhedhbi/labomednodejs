const mongoose = require('mongoose');
const { schema } = require('./userModel');
const Schema = mongoose.Schema;

const chatSchema= new Schema({
   
    message:{
        type: String,
        required:true 
    },
    sender:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user'
    },
    date:{
        type:Date,
        required:true
    }
   
}) 

module.exports=mongoose.model('chat',chatSchema)  