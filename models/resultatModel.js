const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const resultatSchema= new Schema({
    nomLabo:{
        type: String,
        required:true // champ obligatoir
    },
     numOrd:{
        type: Number,
        required:true // champ obligatoir
    },
    dateDepot:{
        type: String,
        required:true
    },

  labo:{
      type:mongoose.Schema.Types.ObjectId,
      ref:'labo'
  }
   
}) 

module.exports=mongoose.model('resultat',resultatSchema)  