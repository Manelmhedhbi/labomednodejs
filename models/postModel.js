const mongoose = require('mongoose');
const userModel = require('../models/userModel')
const Schema = mongoose.Schema;

const postSchema= new Schema({
   
    title:{
        type: String,
        required:true 
    },
    body:{
        type:String,
        required:true
    }, 
    creationDate:{
        type:Date,
        required:false
    }, 
    authorname:{
        type:String,
        required:false
    },
    author:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user'
    },
    comments:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'comment'
    }]
   
}) 

module.exports=mongoose.model('post',postSchema)  