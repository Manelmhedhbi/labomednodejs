const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const laboSchema= new Schema({
    nom:{
        type: String,
        required:true // champ obligatoir
    },
    image:{
        type:String,
        required:true
    },
    phone:{
        type: Number,
        required:true,
      
      
    },
    adress:{
        type: String,
        required:true,
    },
    analyse:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'analyse'
    }],
    resultat:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'resultat'
    }],
  
 
    
}) 

module.exports=mongoose.model('labo',laboSchema)  