const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const ordSchema= new Schema({
   
    numOrd:{
        type: Number,
        required:true // champ obligatoir
    },
    date:{
        type: String,
        required:true
    },
    medecin:{
        type:mongoose.Schema.Types.ObjectId,
        red:'medecin'
    },
    analyse:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'analyse'
    }],
  cons:{
      type:mongoose.Schema.Types.ObjectId,
      ref:'consultation'
  }
  
   
}) 

module.exports=mongoose.model('ordonnance',ordSchema)  