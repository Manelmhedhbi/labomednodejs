const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const facture =new Schema({
    numFac: {
      type: Number,
        required: true
     },
    nomFac: {
          type: String,
            required: true,
            },
    prix:{
        type:Number,
        required: true
    }
    })
const ConsuSchema= new Schema({
   
    
    date:{
        type: String,
        required:true
    },
    nomCon:{
        type: String,
        required:true
    },
    ord:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'ordonnance'
    },
    patient:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'patient'
    },
    facture:[facture],
   
}) 

module.exports=mongoose.model('consultation',ConsuSchema)  