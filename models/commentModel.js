const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema= new Schema({
  
    text:{
        type:String,
        required:true
    }, 
    creationDate:{
        type:Date,
        required:false
    }, 
    creatorName:{
        type:String,
        required:true
    },
    creator:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user'
    },
    postID:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'post'
    }]
   
}) 

module.exports=mongoose.model('comment',commentSchema)  