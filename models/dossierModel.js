const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const dossierSchema= new Schema({
    nomPatient:{
        type: String,
        required:true // champ obligatoir
    },
     prenomPatient:{
        type: String,
        required:true // champ obligatoir
    },
    num:{
        type: Number,
        required:true
    },
dateCreation:{     
        type: String,
        required:true // champ obligatoir
    },
    patient:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'patient'
    },
    resultat:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'resultat'
    }],
    bilan:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'bilan'
    }
  
    
    
   
}) 

module.exports=mongoose.model('dossier',dossierSchema)  //'user': nom de classe dans bd