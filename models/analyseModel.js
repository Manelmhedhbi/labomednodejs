const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const analyseSchema= new Schema({
    nom:{
        type: String,
        required:true // champ obligatoir
    },
    type:{
        type: String,
        required:true // champ obligatoir
    },
    
    prix:{
        type: Number,
        required:true
    },
    
    labo:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'labo'
    },
  
  
   
}) 

module.exports=mongoose.model('analyse',analyseSchema)  