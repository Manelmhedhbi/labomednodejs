const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const bilanSchema= new Schema({
   
    date:{
        type: String,
        required:true
    },
    numOrdonnance:{
        type: Number,
        required:true
    },
      patient:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'patient'
    },

  
   
}) 

module.exports=mongoose.model('bilan',bilanSchema)  