const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const rdvSchema= new Schema({
    heureDeb:{
        type: String,
        required:true // champ obligatoir
    },
     heureFin:{
        type: String,
        required:true // champ obligatoir
    },
    date:{
        type: String,
        required:true
    },
    nom:{
        type: String,
        required:true // champ obligatoir
    },
 
    
    
   
}) 

module.exports=mongoose.model('rdv',rdvSchema) 