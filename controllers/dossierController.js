const dossierModel= require('../models/dossierModel');

module.exports={
  
        add:function(req,res){
        console.log(req.file)
            dossierModel.create({
             nomPatient:req.body.nomPatient,
             prenomPatient:req.body.prenomPatient,
             num:req.body.num,
             dateCreation:req.body.dateCreation,
             bilan:req.body.bilan,
             patient:req.body.patient,
             resultat:req.body.resultat

        },function(err,dossier){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'dossier created', status:200,data:dossier})
            }
         
        })
    },
    getAll:function(req,res){
        dossierModel.find({}).populate('patient').populate('bilan').populate('resultat').exec(function(err,dossiers){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'dossiers in system',status:200,data:dossiers})
            }
        })
    },
    getbyid:function(req,res){
        dossierModel.find({_id:req.params.id}).populate('patient').populate('bilan').populate('resultat').exec(function(err,dossiers){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'dossier in system by id', status:200,data:dossiers})
            }
        })
    },
  
    update:function(req,res){
        dossierModel.findByIdAndUpdate({_id:req.params.id},{$set:{
            nomPatient:req.body.nomPatient,
            prenomPatient:req.body.prenomPatient,
            num:req.body.num,
            dateCreation:req.body.dateCreation,
        
        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,dossier){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'dossier updated', status:200,data:dossier})
            }

        })
    },
    delete:function(req,res){
        dossierModel.findByIdAndDelete({_id:req.params.id},function(err,dossier){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'dossier deleted', status:200})
            }
           })
    },

    pullFunc:function(req,res){     
        dossierModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {bilan : req.body.bilan} },function(err,dossier){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'dossier updated', status:200,data: dossier})
            }
        })
    },
    pushFunc:function(req,res){
        dossierModel.findByIdAndUpdate({_id:req.params.id},{ $push: {bilan : req.body.bilan}},function(err,dossier){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'dossier updated', status:200,data:dossier})
            }
        })
    },
    

    pullR:function(req,res){     
        dossierModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {resultat : req.body.resultat} },function(err,dossier){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'dossier updated', status:200,data: dossier})
            }
        })
    },
    pushR:function(req,res){
        dossierModel.findByIdAndUpdate({_id:req.params.id},{ $push: {resultat : req.body.resultat}},function(err,dossier){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'dossier updated', status:200,data:dossier})
            }
        })
    },
 










}