const analyseModel= require('../models/analyseModel');

module.exports={
  
        add:function(req,res){
        console.log(req.file)
        //nom:req.body.nom,prenom:req...  <==> req.body
        analyseModel.create({
             nom:req.body.nom,
            type:req.body.type,
            prix:req.body.prix,
            labo:req.body.labo,
        },function(err,analyse){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'analyse created', status:200,data:analyse})
            }
         
        })
    },
    getAll:function(req,res){
        analyseModel.find({}).populate('ord').populate('labo').exec(function(err,analyses){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'analyses in system',status:200,data:analyses})
            }
        })
    },
    getbyid:function(req,res){
        analyseModel.find({_id:req.params.id}).populate('ord').populate('labo').exec(function(err,analyses){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'analyses in system by id', status:200,data:analyses})
            }
        })
    },
  
    update:function(req,res){
        analyseModel.findByIdAndUpdate({_id:req.params.id},{$set:{
            nom:req.body.nom,
            type:req.body.type,
            prix:req.body.prix
        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,analyse){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'analyse updated', status:200,data:analyse})
            }

        })
    },
    delete:function(req,res){
        analyseModel.findByIdAndDelete({_id:req.params.id},function(err,analyse){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'analyse deleted', status:200})
            }
           })
    },
 
    pushOrd:function(req,res){
        ordModel.findByIdAndUpdate({_id:req.params.id},{ $push: {ord : req.body.ord} },function(err,ord){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'ord updated', status:200,data:ord})
            }
        })
    },
    pullOrd:function(req,res){     
        ordModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {ord : req.body.ord} },function(err,ord){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'ord updated', status:200,data: ord})
            }
        })
    },
    pushLabo:function(req,res){
        ordModel.findByIdAndUpdate({_id:req.params.id},{ $push: {labo : req.body.labo} },function(err,ord){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'ord updated', status:200,data:ord})
            }
        })
    },
    pullLabo:function(req,res){     
        ordModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {labo : req.body.labo} },function(err,ord){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'ord updated', status:200,data: ord})
            }
        })
    }










}