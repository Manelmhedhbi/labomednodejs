const patientModel= require('../models/patientModel');
const bcrypt= require('bcrypt');
const jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var randtoken = require('rand-token');
const tokenList = {};
const medecinModel=require('../models/medecinModel')
var refreshTokens={};



module.exports={
  
    add:function(req,res){
        console.log(req.file)
        //nom:req.body.nom,prenom:req...  <==> req.body
           patientModel.create({
             nom:req.body.nom,
            prenom:req.body.prenom,
            email:req.body.email,
            password:req.body.password,
            phone:req.body.phone,
            adress:req.body.adress,
            genre:req.body.genre,
            role:req.body.role,
            image:req.file.filename,
            medecin:req.body.medecin,
            dossier:req.body.dossier,
            rdv:req.body.rdv,
            analyse:req.body.analyse,

          

        },function(err,patient){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'patient created', status:200,data:patient})
            }
         
        })
    },
    getAll:function(req,res){
        patientModel.find({}).populate('medecin').populate('dossier').populate('bilan').populate('rdv').exec(function(err,patients){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'patients in system',status:200,data:patients})
            }
        })
    },
    getAllP:function(req,res){
        var size=Number(req.query.size)
        var page= Math.max(0,req.query.page)
        patientModel.find({}).skip(size*page).limit(size).exec(function(err,patients){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'patients in system',status:200,data:patients})
            }
        })
    },
    getbyid:function(req,res){
        patientModel.find({_id:req.params.id}).populate('medecin').populate('dossier').populate('bilan').populate('rdv').exec(function(err,patients){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'patient in system by id', status:200,data:patients})
            }
        })
    },
    getPatient:function(req,res){
        
        medecinModel.findOne({patient:req.query.id}).populate('patient').exec(function(err,patient){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'patient:', status:200,data:patient})
            }
        })
    },
  
  
    update:function(req,res){
        patientModel.findByIdAndUpdate({_id:req.params.id},{$set:{
            nom:req.body.nom,
            prenom:req.body.prenom,
            email:req.body.email,
            phone:req.body.phone,
            adress:req.body.adress,
            genre:req.body.genre
            }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,patient){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'patient updated', status:200,data:patient})
            }

        })
    },
    delete:function(req,res){
        patientModel.findByIdAndDelete({_id:req.params.id},function(err,patient){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'patient deleted', status:200})
            }
           })
    },
    authentification:function(req,res,next){
        patientModel.findOne({email:req.body.email},function(err,userinfo){
            
            if (err){
               next(err)
            }else {
                if(userinfo!=null){
                
                    if(bcrypt.compareSync(req.body.password, userinfo.password)){
                        var refreshToken=randtoken.uid(256)
                        refreshTokens[refreshToken]=userinfo._id
                        console.log('reeeffff', refreshTokens[refreshToken]);
                        console.log('reeeffffaaa', refreshToken in refreshTokens);
                        const token = jwt.sign({id:userinfo._id,role:userinfo.role}, req.app.get('secretkey'),{expiresIn: '1h'});
                       res.json({
                            status:"success",
                            message:"user found",
                            data:{
                            user: userinfo,
                            accesstoken: token,
                            refreshToken: refreshToken}
                        });
                       
                    }else {
                        res.json({status:"error", message:"invalide password", data: null});
                    }
                }
                else {
                    res.json({status:"error",message:"invalid email",data: null});
                }
            }
        })
        },
 
        logOut: function(req,res){
            var refreshToken= req.body.refreshToken
            console.log('refreshtoken', refreshToken)
            jwt.verify(req.headers['x-access-token'],req.app.get('secretkey'))// verify
            if(refreshToken in refreshTokens){
                console.log('hhhhhh',refreshTokens[refreshToken]);
                delete refreshTokens[refreshToken]
            }
            res.json({msg:'token experied', status:204})
            console.log('tttt',refreshTokens[refreshToken]);
       },
    
       pullFunc:function(req,res){     
        patientModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {medecin : req.body.medecin} },function(err,patient){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'patient updated', status:200,data: patient})
            }
        })
    },
    forgotpassword:function(req,res)
       {  
        Email=req.body.email;   
        userModel.findOne({email:Email},(err,user)=>{
        if(err||!user)
         { return res.status(400).json({error:"email does not exist"});}
        var token=jwt.sign({_id:user._id},req.app.get('secretkey'),{expiresIn:'20min'});

        var data = { from: "manelmhe12@gmail.com",
                   to: Email,
                   subject: "activation email",
                   html:`<p>http:127.0.0.1:4200/resetpassword/${token}</p>`
       };
       return userModel.findOneAndUpdate({email:Email},{resetLink:token},(err,succes)=>{
       if(err) { return res.status(400).json({error:"reset password link error"});
       }
       else{
       const transporter = nodemailer.createTransport({
       service: 'gmail',
       auth: {
       user: "manelmhe12@gmail.com",
       pass: 'ma777dikoya'
       }
       });
       transporter.sendMail(data, function(error, info){
       if (error) {
       console.log("ffff",error)
       return res.json({err:"error"})
       } else {
       return res.json({message:"email has been send"});
       }
       });
       }
       })
       })
       },
       refreshToken:function(req,res){
        var id=req.params.id
        var refreshToken=req.body.refreshToken
        if((refreshToken in refreshTokens )&&(refreshTokens[refreshToken]==id)){
            var token =jwt.sign({id:id},req.app.get('secretkey'),{expiresIn:'2min'})
            res.json({token:token})
           }else{
               res.send(401)
           }
    }   ,

    pushFunc:function(req,res){
        patientModel.findByIdAndUpdate({_id:req.params.id},{ $push: {medecin : req.body.medecin}},function(err,patient){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'patient updated', status:200,data:patient})
            }
        })
    },
    pushD:function(req,res){
        patientModel.findByIdAndUpdate({_id:req.params.id},{ $push: {dossier : req.body.dossier} },function(err,patient){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'patient updated', status:200,data:patient})
            }
        })
    },
    pullD:function(req,res){     
        patientModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {dossier : req.body.dossier} },function(err,patient){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'patient updated', status:200,data: patient})
            }
        })
    },

 













    
}