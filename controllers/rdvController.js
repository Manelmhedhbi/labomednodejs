const rdvModel= require('../models/rdvModel');

module.exports={
  
        add:function(req,res){
        console.log(req.file)
            rdvModel.create({
             heureDeb:req.body.heureDeb,
             heureFin:req.body.heureFin,
             date:req.body.date,
             nom:req.body.nom,

             

        },function(err,rdv){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'rdv created', status:200,data:rdv})
            }
         
        })
    },
    getAll:function(req,res){
        rdvModel.find({}).exec(function(err,rdvs){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'rdvs in system',status:200,data:rdvs})
            }
        })
    },
    getbyid:function(req,res){
        rdvModel.find({_id:req.params.id}).exec(function(err,rdvs){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'rdv in system by id', status:200,data:rdvs})
            }
        })
    },
  
    update:function(req,res){
        rdvModel.findByIdAndUpdate({_id:req.params.id},{$set:{
            heureDeb:req.body.heureDeb,
             heureFin:req.body.heureFin,
             date:req.body.date,
             nom:req.body.nom

             
        
        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,rdv){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'rdv updated', status:200,data:rdv})
            }

        })
    },
    delete:function(req,res){
        rdvModel.findByIdAndDelete({_id:req.params.id},function(err,rdv){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'rdv deleted', status:200})
            }
           })
    },
    getbynom:function(req,res){
        rdvModel.find({nom:req.params.nom}).exec(function(err,rdvs){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'rdv in system by nom', status:200,data:rdvs})
            }
        })
    },
  
 










}