const laboModel= require('../models/laboModel');
var nodemailer = require('nodemailer');



module.exports={
  
        add:function(req,res){
        console.log(req.file)
            laboModel.create({
            image:req.file.filename,
             nom:req.body.nom,
            phone:req.body.phone,
            adress:req.body.adress,
            analyse:req.body.analyse,
            resultat:req.body.resultat
      


        },function(err,labo){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'labo created', status:200,data:labo})
            }
         
        })
    },
    getAll:function(req,res){
        laboModel.find({}).populate('analyse').exec(function(err,labos){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'labos in system',status:200,data:labos})
            }
        })
    },
    getbyid:function(req,res){
        laboModel.find({_id:req.params.id}).populate('analyse').exec(function(err,labos){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'labo in system by id', status:200,data:labos})
            }
        })
    },
  
    update:function(req,res){
        laboModel.findByIdAndUpdate({_id:req.params.id},{$set:{
            nom:req.body.nom,
            phone:req.body.phone,
            adress:req.body.adress,
            image:req.file.filename
        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,labo){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'labo updated', status:200,data:labo})
            }

        })
    },
    delete:function(req,res){
        laboModel.findByIdAndDelete({_id:req.params.id},function(err,labo){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'labo deleted', status:200})
            }
           })
    },
    pushA:function(req,res){
        laboModel.findByIdAndUpdate({_id:req.params.id},{ $push: {analyse : req.body.analyse} },function(err,labo){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'labo updated', status:200,data:labo})
            }
        })
    },
    pullA:function(req,res){     
        laboModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {analyse : req.body.analyse} },function(err,labo){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'labo updated', status:200,data: labo})
            }
        })
    }




}