const resModel= require('../models/resultatModel');

module.exports={
  
        add:function(req,res){
        console.log(req.file)
        resModel.create({
             nomLabo:req.body.nomLabo,
             numOrd:req.body.numOrd,
             dateDepot:req.body.dateDepot,
             labo:req.body.labo

        },function(err,resultat){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'resultat created', status:200,data:resultat})
            }
         
        })
    },
    getAll:function(req,res){
        resModel.find({}).populate('labo').exec(function(err,resultats){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'resultats in system',status:200,data:resultats})
            }
        })
    },
    getbyid:function(req,res){
        resModel.find({_id:req.params.id}).exec(function(err,resultats){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'resultat in system by id', status:200,data:resultats})
            }
        })
    },
  
    update:function(req,res){
        resModel.findByIdAndUpdate({_id:req.params.id}.populate('labo'),{$set:{
            nomLabo:req.body.nomLabo,
             numOrd:req.body.numOrd,
             dateDepot:req.body.dateDepot,

        
        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,resultat){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'resultat updated', status:200,data:resultat})
            }

        })
    },
    delete:function(req,res){
        resModel.findByIdAndDelete({_id:req.params.id},function(err){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'resultat deleted', status:200})
            }
           })
    },

   
 










}