const userModel= require('../models/userModel');
const secretaireModel=require('../models/secretaireModel');
const bcrypt= require('bcrypt');
const jwt = require('jsonwebtoken');
var { htmlToText } = require('html-to-text');
var nodemailer = require('nodemailer');
var randtoken = require('rand-token');
var _=require('lodash');
const { text } = require('express');
const tokenList = {};
var refreshTokens={};
var parPage=4;



module.exports={
  
        add:function(req,res){
        console.log(req.file)
        //nom:req.body.nom,prenom:req...  <==> req.body
            userModel.create({
            image:req.file.filename,
             nom:req.body.nom,
            prenom:req.body.prenom,
            email:req.body.email,
            password:req.body.password,
            phone:req.body.phone,
            adress:req.body.adress,
            genre:req.body.genre,
            role:req.body.role,


        },function(err,user){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'user created', status:200,data:user})
            }
         
        })
    },
    getAll:function(req,res){
        userModel.find({}).exec(function(err,Users){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'users in system',status:200,data:Users})
            }
        })
    },
    getbyid:function(req,res){
        userModel.find({_id:req.params.id}).exec(function(err,Users){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'user in system by id', status:200,data:Users})
            }
        })
    },
  
    update:function(req,res){
        userModel.findByIdAndUpdate({_id:req.params.id},{$set:{
            nom:req.body.nom,
            prenom:req.body.prenom,
            email:req.body.email,
            password:req.body.password,
            phone:req.body.phone,
            adress:req.body.adress,
            genre:req.body.genre,
            image:req.file.filename
        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,User){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'user updated', status:200,data:User})
            }

        })
    },
    delete:function(req,res){
        userModel.findByIdAndDelete({_id:req.params.id},function(err,User){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'user deleted', status:200})
            }
           })
    },
    authentification:function(req,res,next){
        userModel.findOne({email:req.body.email},function(err,userinfo){
            
            if (err){
               next(err)
            }else {
                if(userinfo!=null){
                
                    if(bcrypt.compareSync(req.body.password, userinfo.password)){
                        var refreshToken=randtoken.uid(256)
                        refreshTokens[refreshToken]=userinfo._id
                        console.log('reeeffff', refreshTokens[refreshToken]);
                        console.log('reeeffffaaa', refreshToken in refreshTokens);
                        const token = jwt.sign({id:userinfo._id, role:userinfo.role },
                             req.app.get('secretkey'),{expiresIn: '1h'});
                       res.json({
                            status:"success",
                            message:"user found",
                            data:{
                            user: userinfo,
                            accesstoken: token,
                            refreshToken: refreshToken}
                        });
                       
                    }else {
                        res.json({status:"error", message:"invalide password", data: null});
                    }
                }
                else {
                    res.json({status:"error",message:"invalid email",data: null});
                }
            }
        })
        },
      //  refreshToken:function(req,res){
      //      var id = req.body._id  // id ta3 chnouwa??? userId
        //    var refreshToken=req.body.refreshToken
          //  console.log('id',req.body._id);
            //console.log('refreshToken',req.body.refreshToken)
            //console.log('refreshToken',req.body.refreshToken)
            //console.log('test1',refreshToken in refreshTokens)
            //console.log('test2',refreshTokens[refreshToken]==id)

//            if ((refreshToken in refreshTokens)&& (refreshTokens[refreshToken]==id)){
  //              var user ={
    //                'id':id
      //          }
         //       var token = jwt.sign(user,req.app.get('secretkey'),{expiresIn:3600})// ?
           // res.json({accesstoken:token})          
       // } else {
         //   res.sendStatus(401)
        //}
        //},
      

        refreshToken:function(req,res){
            var id=req.params.id
            var refreshToken=req.body.refreshToken
            if((refreshToken in refreshTokens )&&(refreshTokens[refreshToken]==id)){
                var token =jwt.sign({id:id},req.app.get('secretkey'),{expiresIn:'2min'})
                res.json({token:token})
               }else{
                   res.send(401)
               }
        }   ,


        logOut: function(req,res){
            var refreshToken= req.body.refreshToken
            console.log('refreshtoken', refreshToken)
            jwt.verify(req.headers['x-access-token'],req.app.get('secretkey'))// verify
            if(refreshToken in refreshTokens){
                console.log('hhhhhh',refreshTokens[refreshToken]);
                delete refreshTokens[refreshToken]
            }
            res.json({msg:'token experied', status:204})
            console.log('tttt',refreshTokens[refreshToken]);
       },
       
       forgotpassword:function(req,res)
       {  
        Email=req.body.email;   
        userModel.findOne({email:Email},(err,user)=>{
        if(err||!user)
         { return res.status(400).json({error:"email does not exist"});}
        var token=jwt.sign({_id:user._id},req.app.get('secretkey'),{expiresIn:'20min'});

        var data = { from: "manelmhe12@gmail.com",
                   to: Email,
                   subject: "activation email",
                   html:`<p>http:127.0.0.1:4200/resetpassword/${token}</p>`
       };
       return userModel.findOneAndUpdate({email:Email},{resetLink:token},(err,succes)=>{
       if(err) { return res.status(400).json({error:"reset password link error"});
       }
       else{
       const transporter = nodemailer.createTransport({
       service: 'gmail',
       auth: {
       user: "manelmhe12@gmail.com",
       pass: 'ma777dikoya'
       }
       });
       transporter.sendMail(data, function(error, info){
       if (error) {
       console.log("ffff",error)
       return res.json({err:"error"})
       } else {
       return res.json({message:"email has been send"});
       }
       });
       }
       })
       })
       },
       
       
       
resetpassword:function(req,res)
{
resetLink=req.body.resetLink
newPass=req.body.newPass
if(resetLink)
{
jwt.verify(resetLink,req.app.get('secretkey'),function(err,decodeData)
{
if(err)
{
return res.json({
error:"incorrect token or it is exprired"
})
}
userModel.findOne({resetLink},(err,user)=>{
if(err||!user)
{
return res.json({error:"user with this token does not exist"});
}
const obj={
password:newPass
}
user=_.extend(user,obj);
user.save((err,result)=>{
if(err)
{
return res.status(400).json({error:"reset password error"});
}
else
{
return res.status(200).json({message:"password has been changed"});
}
})
})
})
}
else
{
return res.status(401).json({error:"authentification erreur"});
}
},
sendmail : function(req,res){
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
        user: "manelmhe12@gmail.com",
        pass: 'ma777dikoya'
        }
        });
        var data = { 
                   from: req.body.from,
                   to: req.body.to,
                   subject:req.body.subject,
                   html:req.body.text
                  // text: htmlToText(req.body.text, {
                    //formatters: {
                      // Create a formatter.
                      //'fooBlockFormatter': function (elem, walk, builder, formatOptions) {
                        //builder.openBlock(formatOptions.leadingLineBreaks || 1);
                        //walk(elem.children, builder);
                        //builder.addInline('!');
                        //builder.closeBlock(formatOptions.trailingLineBreaks || 1);
                      //}
                    //},
                    //tags: {
                      // Assign it to `foo` tags.
                      //'foo': {
                        //format: 'fooBlockFormatter',
                        //options: { leadingLineBreaks: 1, trailingLineBreaks: 1 },
                        //'a': { options: { baseUrl: 'https://example.com' } },
                        //'img': { options: { baseUrl: 'https://example.com' } },

                      //}}})
        };
        transporter.sendMail(data, function(error, info){
        if (error) {
        console.log("ffff",error)
        return res.json({err:"error"})
        } else {
        return res.json({message:"email has been send"});
        }
        });

}




































}