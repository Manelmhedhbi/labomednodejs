const bilanModel= require('../models/bilanModel');

module.exports={
  
        add:function(req,res){
        console.log(req.file)
        //nom:req.body.nom,prenom:req...  <==> req.body
        bilanModel.create({
            
            date:req.body.date,
            numOrdonnance:req.body.numOrdonnance,
            patient:req.body.patient
        },function(err,bilan){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'bilan created', status:200,data:bilan})
            }
         
        })
    },
    getAll:function(req,res){
        bilanModel.find({}).exec(function(err,bilans){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'bilans in system',status:200,data:bilans})
            }
        })
    },
    getbyid:function(req,res){
        bilanModel.find({_id:req.params.id}).exec(function(err,bilans){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'bilans in system by id', status:200,data:bilans})
            }
        })
    },
  
    update:function(req,res){
        bilanModel.findByIdAndUpdate({_id:req.params.id},{$set:{
           
            date:req.body.date,
            numOrdonnance:req.body.numOrdonnance,
        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,bilan){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'bilan updated', status:200,data:bilan})
            }

        })
    },
    delete:function(req,res){
        bilanModel.findByIdAndDelete({_id:req.params.id},function(err,bilan){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'bilan deleted', status:200})
            }
           })
    },
 










}