const secretModel= require('../models/secretaireModel');
const bcrypt= require('bcrypt');
const jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var randtoken = require('rand-token');
const tokenList = {};
var refreshTokens={};



module.exports={
  
    add:function(req,res){
        console.log(req.file)
        //nom:req.body.nom,prenom:req...  <==> req.body
           secretModel.create({
             nom:req.body.nom,
            prenom:req.body.prenom,
            email:req.body.email,
            password:req.body.password,
            phone:req.body.phone,
            adress:req.body.adress,
            genre:req.body.genre,
       
            image:req.file.filename,
           

          

        },function(err,sec){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'sec created', status:200,data:sec})
            }
         
        })
    },
    getAll:function(req,res){
        secretModel.find({}).exec(function(err,users){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'secretaires in system',status:200,data:users})
            }
        })
    },
    getbyid:function(req,res){
        secretModel.find({_id:req.params.id}).exec(function(err,userss){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'secretaires in system by id', status:200,data:userss})
            }
        })
    },
  
    update:function(req,res){
        secretModel.findByIdAndUpdate({_id:req.params.id},{$set:{
            nom:req.body.nom,
            prenom:req.body.prenom,
            email:req.body.email,
            password:req.body.password,
            phone:req.body.phone,
            adress:req.body.adress,
            genre:req.body.genre,
            image:req.file.filename        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,sec){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'sec updated', status:200,data:sec})
            }

        })
    },
    delete:function(req,res){
        secretModel.findByIdAndDelete({_id:req.params.id},function(err,user){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'secretaire deleted', status:200})
            }
           })
    },
    authentification:function(req,res,next){
        secretModel.findOne({email:req.body.email},function(err,userinfo){
            
            if (err){
               next(err)
            }else {
                if(userinfo!=null){
                
                    if(bcrypt.compareSync(req.body.password, userinfo.password)){
                        var refreshToken=randtoken.uid(256)
                        refreshTokens[refreshToken]=userinfo._id
                        console.log('reeeffff', refreshTokens[refreshToken]);
                        console.log('reeeffffaaa', refreshToken in refreshTokens);
                        const token = jwt.sign({id:userinfo._id }, req.app.get('secretkey'),{expiresIn: '1h'});
                       res.json({
                            status:"success",
                            message:"user found",
                            data:{
                            user: userinfo,
                            accesstoken: token,
                            refreshToken: refreshToken}
                        });
                       
                    }else {
                        res.json({status:"error", message:"invalide password", data: null});
                    }
                }
                else {
                    res.json({status:"error",message:"invalid email",data: null});
                }
            }
        })
        },
        refreshToken:function(req,res){
            var id = req.body._id  // id ta3 chnouwa??? userId
            var refreshToken=req.body.refreshToken
            console.log('id',req.body._id);
            console.log('refreshToken',req.body.refreshToken)
            console.log('refreshToken',req.body.refreshToken)
            console.log('test1',refreshToken in refreshTokens)
            console.log('test2',refreshTokens[refreshToken]==id)

            if ((refreshToken in refreshTokens)&& (refreshTokens[refreshToken]==id)){
                var user ={
                    'id':id
                }
                var token = jwt.sign(user,req.app.get('secretkey'),{expiresIn:3600})// ?
            res.json({accesstoken:token})          
        } else {
            res.sendStatus(401)
        }
        },
        logOut: function(req,res){
            var refreshToken= req.body.refreshToken
            console.log('refreshtoken', refreshToken)
            jwt.verify(req.headers['x-access-token'],req.app.get('secretkey'))// verify
            if(refreshToken in refreshTokens){
                console.log('hhhhhh',refreshTokens[refreshToken]);
                delete refreshTokens[refreshToken]
            }
            res.json({msg:'token experied', status:204})
            console.log('tttt',refreshTokens[refreshToken]);
       },
    
       pullFunc:function(req,res){     
        secretModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {medecin : req.body.medecin} },function(err,user){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'secretaires updated', status:200,data: user})
            }
        })
    },
    pushFunc:function(req,res){
        secretModel.findByIdAndUpdate({_id:req.params.id},{ $push: {medecin : req.body.medecin}},function(err,user){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'secretaire updated', status:200,data:user})
            }
        })
    },
    pushD:function(req,res){
        secretModel.findByIdAndUpdate({_id:req.params.id},{ $push: {dossier : req.body.dossier} },function(err,user){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'secretaire updated', status:200,data:user})
            }
        })
    },
    pullD:function(req,res){     
        secretModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {dossier : req.body.dossier} },function(err,user){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'user updated', status:200,data: user})
            }
        })
    }















    
}