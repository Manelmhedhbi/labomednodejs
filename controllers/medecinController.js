const medecinModel = require('../models/medecinModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var randtoken = require('rand-token');
const { toNumber } = require('lodash');
const tokenList = {};
var refreshTokens = {};
var parPage = 2;





module.exports = {

    add: function (req, res) {
        console.log(req.file)
        //nom:req.body.nom,prenom:req...  <==> req.body
        medecinModel.create({
            nom: req.body.nom,
            prenom: req.body.prenom,
            email: req.body.email,
            password: req.body.password,
            phone: req.body.phone,
            adress: req.body.adress,
            genre: req.body.genre,
            role: req.body.role,
            patient: req.body.patient,
            image: req.file.filename


        }, function (err, medecin) {
            if (err) {
                res.json({ message: 'error' + err, status: 500, data: null })
            }
            else {
                res.json({ message: 'medecin created', status: 200, data: medecin })
            }

        })
    },
    getAll: function (req, res) {
        const n = Number(req.query.page)
        var page = Math.max(0, n)

        medecinModel.find({}).populate('patient').skip(parPage * page).limit(parPage).
            exec(function (err, medecins) {
                if (err) {
                    res.json({ message: 'error', status: 500, data: null })
                }
                else {
                    res.json({ message: 'medecins in system', status: 200, data: medecins })
                }
            })
    },
    getbyid: function (req, res) {
        medecinModel.find({ _id: req.params.id }).populate('patient').exec(function (err, medecins) {
            if (err) {
                res.json({ message: 'error' + err, status: 500, data: null })
            }
            else {
                res.json(medecins)
            }
        })
    },
    getMyPatients: function (req, res) {
        medecinModel.findone({ patient: req.body.patient }.exec(function (err, medecins) {
            if (err) {
                res.json({ message: 'error' + err, status: 500, data: null })
            }
            res.json({ message: 'medecin in system by id', status: 200, data: medecins })

        }))
    },

    update: function (req, res) {
        medecinModel.findByIdAndUpdate({ _id: req.params.id }, {
            $set: {
                nom: req.body.nom,
                prenom: req.body.prenom,
                email: req.body.email,
                password: req.body.password,
                phone: req.body.phone,
                adress: req.body.adress,
                genre: req.body.genre,
                image: req.file.filename
            }
        }, {
            runValidators: true, upsert: true,
            setDefaultsOnInsert: true, new: true
        }, function (err, medecin) {
            if (err) {
                res.json({ message: 'error' + err, status: 500, data: null })
            }
            else {
                res.json({ message: 'medecin updated', status: 200, data: medecin })
            }

        })
    },
    delete: function (req, res) {
        medecinModel.findByIdAndDelete({ _id: req.params.id }, function (err, medecin) {
            if (err) {
                res.json({ message: 'error', status: 500, data: null })
            }
            else {
                res.json({ message: 'medecin deleted', status: 200 })
            }
        })
    },
    authentification: function (req, res, next) {
        medecinModel.findOne({ email: req.body.email }, function (err, userinfo) {
            if (err) {
                next(err)
            } else {
                if (userinfo != null) {

                    if (bcrypt.compareSync(req.body.password, userinfo.password)) {
                        var refreshToken = randtoken.uid(256)
                        refreshTokens[refreshToken] = userinfo._id
                        console.log('reeeffff', refreshTokens[refreshToken]);
                        console.log('reeeffffaaa', refreshToken in refreshTokens);
                        const token = jwt.sign({ id: userinfo._id, role: userinfo.role },
                            req.app.get('secretkey'), { expiresIn: '1h' });
                        res.json({
                            status: "success",
                            message: "user found",
                            data: {
                                user: userinfo,
                                accesstoken: token,
                                refreshToken: refreshToken
                            }
                        });

                    } else {
                        res.json({ status: "error", message: "invalide password", data: null });
                    }
                }
                else {
                    res.json({ status: "error", message: "invalid email", data: null });
                }
            }
        })
    },
    refreshToken: function (req, res) {
        var id = req.body._id  // id ta3 chnouwa??? userId
        var refreshToken = req.body.refreshToken
        console.log('id', req.body._id);
        console.log('refreshToken', req.body.refreshToken)
        console.log('refreshToken', req.body.refreshToken)
        console.log('test1', refreshToken in refreshTokens)
        console.log('test2', refreshTokens[refreshToken] == id)

        if ((refreshToken in refreshTokens) && (refreshTokens[refreshToken] == id)) {
            var user = {
                'id': id
            }
            var token = jwt.sign(user, req.app.get('secretkey'), { expiresIn: 3600 })// ?
            res.json({ accesstoken: token })
        } else {
            res.sendStatus(401)
        }
    },
    logOut: function (req, res) {
        var refreshToken = req.body.refreshToken
        console.log('refreshtoken', refreshToken)
        jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'))// verify
        if (refreshToken in refreshTokens) {
            console.log('hhhhhh', refreshTokens[refreshToken]);
            delete refreshTokens[refreshToken]
        }
        res.json({ msg: 'token experied', status: 204 })
        console.log('tttt', refreshTokens[refreshToken]);
    },

    pullFunc: function (req, res) {
        medecinModel.findByIdAndUpdate({ _id: req.params.id }, { $pull: { patient: req.body.patient } }, function (err, medecin) {
            if (err) {
                res.json({ message: 'error', status: 500, data: null })
            } else {
                res.json({ message: 'medecin updated', status: 200, data: medecin })
            }
        })
    },
    pushFunc: function (req, res) {
        medecinModel.findByIdAndUpdate({ _id: req.params.id }, { $push: { patient: req.body.patient } }, function (err, medecin) {
            if (err) {
                res.json({ message: 'error', status: 500, data: null })
            } else {
                res.json({ message: 'medecin updated', status: 200, data: medecin })
            }
        })
    },

































}