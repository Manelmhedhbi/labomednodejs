const commentModel=require('../models/commentModel')
const postModel=require('../models/postModel')

module.exports={

addComment:async (req, res) => {
    const commentObj = req.body
    //commentObj.creator = req.user._id
    commentModel
      .create(commentObj)
      .then((createdComment) => {
        postModel
          .findById(createdComment.postID)
          .then(post => {
            let postComments = post.comments
            postComments.push(createdComment._id)
            post.comments = postComments
            post
              .save()
              .then(() => {
                res.status(200).json({
                  success: true,
                  message: 'Comment created successfully',
                  data: createdComment
                })
              })
              .catch((err) => {
                console.log(err)
                const message = 'Something went wrong :('
                return res.status(401).json({
                  success: false,
                  message: message
                })
              })
          })
      })
      .catch((err) => {
        console.log(err)
        const message = 'Something went wrong :('
        return res.status(401).json({
          success: false,
          message: message
        })
      })
  },

 updateComment: async (req, res) => {
    const commentId = req.params.id
    let existingComment = await commentModel.findById(commentId)
      .catch((err) => {
        console.log(err)
        const message = 'Something went wrong :( Check the form for errors.'
        return res.status(401).json({
          success: false,
          message: message
        })
      })
   // if (req.user._id.toString() === existingComment.creator.toString() || req.user.roles.indexOf('Admin') > -1) {
      const commentObj = req.body
      existingComment.text = commentObj.text
      existingComment.creationDate = Date.now()
      existingComment
        .save()
        .then(editedComment => {
          res.status(200).json({
            success: true,
            message: 'Comment Edited Successfully.',
            data: editedComment
          })
        })
        .catch((err) => {
          console.log(err)
          let message = 'Something went wrong :( Check the form for errors.'
          return res.status(401).json({
            success: false,
            message: message
          })
        })
     
  },
deleteComment: async (req, res) => {
    const commentId = req.params.id
    let comment = await commentModel.findById(commentId)
      .catch((err) => {
        console.log(err)
        const message = 'Entry does not exist!'
        return res.status(401).json({
          success: false,
          message: message
        })
      })
   // if (req.user._id.toString() === comment.creator.toString() || req.user.roles.includes('Admin') > -1) {
      let post = await postModel.findById(comment.postID)
      console.log(post.comments)
      let postComments = post.comments.filter(c => c.toString() !== commentId)
      post.comments = postComments
      await post.save()
      comment
        .remove()
        .then(() => {
          return res.status(200).json({
            success: true,
            message: 'Comment deleted successfully!'
          })
        })
        .catch((err) => {
          console.log(err)
          return res.status(401).json({
            success: false,
            message: 'Something went wrong :('
          })
        })
    // } else {
    //   return res.status(401).json({
    //     success: false,
    //     message: 'Invalid credentials'
    //   })
    }



















}


















