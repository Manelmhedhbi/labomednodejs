const chatModel= require('../models/chatModel');

module.exports={
  
        add:function(req,res){
        console.log(req.file)
        chatModel.create({
             message:req.body.message,
             sender:req.body.sender,
        },function(err,msg){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'msg created', status:200,data:msg})
            }
         
        })
    },
    getAll:function(req,res){
        chatModel.find({}).populate('sender').exec(function(err,msgs){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'msgs in system',status:200,data:msgs})
            }
        })
    },
 
   
    delete:function(req,res){
        chatModel.findByIdAndDelete({_id:req.params.id},function(err,msg){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'msg deleted', status:200})
            }
           })
    },
  

}