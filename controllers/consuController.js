const consModel= require('../models/consuModel');

module.exports={
  
        add:function(req,res){
        console.log(req.file)
        consModel.create({
             
             patient:req.body.patient,
             nomCon:req.body.nomCon,
             date:req.body.date,
             ord:req.body.ord,
             facture:req.body.facture

        },function(err,consultation){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'consultation created', status:200,data:consultation})
            }
         
        })
    },
    getAll:function(req,res){
        consModel.find({}).exec(function(err,consultations){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'consultations in system',status:200,data:consultations})
            }
        })
    },
    getbyid:function(req,res){
        consModel.find({_id:req.params.id}).exec(function(err,consultations){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'consultation in system by id', status:200,data:consultations})
            }
        })
    },
  
    update:function(req,res){
        consModel.findByIdAndUpdate({_id:req.params.id},{$set:{
          
            nomCons:req.body.nomCons,
            date:req.body.date,
        
        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,consultation){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'consultation updated', status:200,data:consultation})
            }

        })
    },
    delete:function(req,res){
        consModel.findByIdAndDelete({_id:req.params.id},function(err){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'consultation deleted', status:200})
            }
           })
    },

    pullFunc:function(req,res){     
        consModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {fichier : req.body.fichier} },function(err,consultation){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'consultation updated', status:200,data: consultation})
            }
        })
    },
    pushFunc:function(req,res){
        consModel.findByIdAndUpdate({_id:req.params.id},{ $push: {fichier : req.body.fichier}},function(err,consultation){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'consultation updated', status:200,data:consultation})
            }
        })
    },
 










}