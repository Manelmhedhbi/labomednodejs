const ordModel= require('../models/ordModel');

module.exports={
  
        add:function(req,res){
        console.log(req.file)
        //nom:req.body.nom,prenom:req...  <==> req.body
        ordModel.create({
             medecin:req.body.medecin,
            numOrd:req.body.numOrd,
            date:req.body.date,
            analyse:req.body.analyse,
            cons:req.body.cons
        },function(err,ord){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else {
                res.json({message:'ord created', status:200,data:ord})
            }
         
        })
    },
    getAll:function(req,res){
        ordModel.find({}).populate('analyse').exec(function(err,ords){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'ords in system',status:200,data:ords})
            }
        })
    },
    getbyid:function(req,res){
        ordModel.find({_id:req.params.id}).populate('analyse').exec(function(err,ords){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'ords in system by id', status:200,data:ords})
            }
        })
    },
  
    update:function(req,res){
        ordModel.findByIdAndUpdate({_id:req.params.id},{$set:{
            nomMed:req.body.nomMed,
            numOrd:req.body.numOrd,
            date:req.body.date
        }},{runValidators: true, upsert: true, 
            setDefaultsOnInsert: true, new: true},function(err,ord){
            if(err){
                res.json({message:'error'+err,status:500,data:null})
            }
            else{
                res.json({message:'ord updated', status:200,data:ord})
            }

        })
    },
    delete:function(req,res){
        ordModel.findByIdAndDelete({_id:req.params.id},function(err,ord){
            if(err){
                res.json({message:'error',status:500,data:null})
            }
            else{
                res.json({message:'ord deleted', status:200})
            }
           })
    },
    pushA:function(req,res){
        ordModel.findByIdAndUpdate({_id:req.params.id},{ $push: {analyse : req.body.analyse} },function(err,ord){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'ord updated', status:200,data:ord})
            }
        })
    },
    pullA:function(req,res){     
        ordModel.findByIdAndUpdate({_id:req.params.id},{ $pull: {analyse : req.body.analyse} },function(err,ord){
            if (err) {
                res.json({message:'error',status:500,data:null})
                } else {
                    res.json({message:'ord updated', status:200,data: ord})
            }
        })
    }











}