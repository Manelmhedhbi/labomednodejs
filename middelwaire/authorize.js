const jwt=require('jsonwebtoken');

module.exports={ 

IsPatient:function(req,res,next) {
       const roleData="patient"
    // if (typeof roleData === 'string') {
    //     roleData = roleData;
    // }
    jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'),function(err, decoded) {
        if (err) {
         return res.status(401).json({status:"error", message: err.message, data:null});
        }else{
          userId = decoded._id;
          role = decoded.role;
          console.log(role);

          if (role && role!==roleData) {
            return res.status(401).json({ message: 'Unauthorized' });
        }
          next();
        }
      });
},

isRole:function(roles = []) {
   
 return(req,res,next)=>{
 if (typeof roles === 'string') {
     roles = [roles];
    
    }

jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'),function(err, decoded) {
   if (err) {
    return res.status(401).json({status:"error", message: err.message, data:null});
   }else{
     userId = decoded._id;
     role = decoded.role;

     if (roles.length && !roles.includes(role)) {
       return res.status(401).json({ message: 'Unauthorized' });
   }
     next();
   }
 });


 }


}

}