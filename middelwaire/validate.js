const userModel= require('../models/userModel');
const jwt = require('jsonwebtoken');
 module.exports=  {
    isLoggedIn:function (req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'), function(err, decoded) {
      if (err) {
        res.status(401).json({status:"error", message: err.message, data:null});
      }else{
        // add user id to request
        req.body.userId = decoded.id;
        next();
      }
    });
  }
}