const laboController= require('../controllers/laboController')
const express= require('express')
const route = express.Router()//router gere routage
const file = require('../middelwaire/file')

route.post('/add',file.single('image'),laboController.add)   
route.get('/getall',laboController.getAll)
route.get('/getbyid/:id',laboController.getbyid)
route.put('/update/:id',file.single('image'),laboController.update)
route.delete('/delete/:id',laboController.delete)
module.exports=route
