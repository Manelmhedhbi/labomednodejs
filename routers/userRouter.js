const userController= require('../controllers/userController')
const express= require('express')
const route = express.Router()//router gere routage
const file = require('../middelwaire/file')
const validate=require('../middelwaire/validate')
const aut=require('../middelwaire/authorize')
const Role=require('../middelwaire/role')
const { Router } = require('express')


route.post('/add',file.single('image'),userController.add)   
route.get('/getall',aut.IsPatient,userController.getAll)
route.get('/getbyid/:id',userController.getbyid)
route.put('/update/:id',file.single('image'),userController.update)
route.delete('/delete/:id',userController.delete)
route.post('/authe',userController.authentification)
route.post('/logout',validate.isLoggedIn,userController.logOut)
route.post('/forgetP',validate.isLoggedIn,userController.forgotpassword)
route.post('/resetP',validate.isLoggedIn,userController.resetpassword)
route.post('/reft/:id',userController.refreshToken)
route.post('/sendmail',userController.sendmail)


module.exports=route
