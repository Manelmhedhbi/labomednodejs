const rdvController= require('../controllers/rdvController')
const express= require('express')
const route = express.Router()//router gere routage


route.post('/add',rdvController.add)   
route.get('/getall',rdvController.getAll)
route.get('/getbyid/:id',rdvController.getbyid)
route.get('/getbynom/:nom',rdvController.getbynom)

route.put('/update/:id',rdvController.update)
route.delete('/delete/:id',rdvController.delete)

module.exports=route
