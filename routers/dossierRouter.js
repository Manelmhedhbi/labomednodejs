const dossierController= require('../controllers/dossierController')
const express= require('express')
const route = express.Router()//router gere routage


route.post('/add',dossierController.add)   
route.get('/getall',dossierController.getAll)
route.get('/getbyid/:id',dossierController.getbyid)
route.put('/update/:id',dossierController.update)
route.delete('/delete/:id',dossierController.delete)

module.exports=route
