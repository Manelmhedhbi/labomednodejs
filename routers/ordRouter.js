const ordController= require('../controllers/ordController')
const express= require('express')
const route = express.Router()//router gere routage


route.post('/add',ordController.add)   
route.get('/getall',ordController.getAll)
route.get('/getbyid/:id',ordController.getbyid)
route.put('/update/:id',ordController.update)
route.delete('/delete/:id',ordController.delete)
module.exports=route
