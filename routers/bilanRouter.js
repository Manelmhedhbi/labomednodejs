
const bilanController= require('../controllers/bilanController')
const express= require('express')
const route = express.Router()


route.post('/add',bilanController.add)   
route.get('/getall',bilanController.getAll)
route.get('/getbyid/:id',bilanController.getbyid)
route.put('/update/:id',bilanController.update)
route.delete('/delete/:id',bilanController.delete)

module.exports=route
