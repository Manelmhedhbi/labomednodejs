const medecinController = require('../controllers/medecinController')
const express= require('express')
const route = express.Router()//router gere routage
const file = require('../middelwaire/file')
const jwt = require('jsonwebtoken')


route.post('/add',file.single('image'),medecinController.add)   
route.get('/getall',medecinController.getAll)
route.get('/getbyid/:id',medecinController.getbyid)
route.get('/getP/:id',medecinController.getMyPatients)
route.put('/update/:id',file.single('image'),medecinController.update)
route.delete('/delete/:id',medecinController.delete)
route.post('/authe',medecinController.authentification)
route.post('/logout',medecinController.logOut)
route.put('/pull/:id',medecinController.pullFunc)
route.put('/push/:id',medecinController.pushFunc)
module.exports=route