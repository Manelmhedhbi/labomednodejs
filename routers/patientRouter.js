const patientController = require('../controllers/patientController')
const express= require('express')
const route = express.Router()//router gere routage
const file = require('../middelwaire/file')
const validate=require('../middelwaire/validate')
const jwt = require('jsonwebtoken')
const   AccessControl =require('accesscontrol');
const patientSchema =require('../models/patientModel')
const { isLoggedIn } = require('../middelwaire/validate')
const { isRole } = require('../middelwaire/authorize')
const authorize = require('../middelwaire/authorize')


const {ac}=require('../rbca')

// const ac = new AccessControl();
// ac.grant('medecin').readAny('get').updateAny('get').deleteAny('get');
// ac.grant('user').readAny('post').updateAny('post').deleteAny('post');


route.post('/add',file.single('image'),patientController.add)   
route.get('/getall',authorize.isRole(["medecin","patient"]),patientController.getAll)
route.get('/getallp',patientController.getAllP)
route.get('/getbyid/:id',patientController.getbyid)
route.put('/update/:id',patientController.update)
route.delete('/delete/:id',patientController.delete)
route.post('/authe',patientController.authentification)
route.post('/logout',validate.isLoggedIn,patientController.logOut)
route.put('/pull/:id',patientController.pullFunc)
route.put('/push/:id',patientController.pushFunc)
route.put('/pulld/:id',patientController.pullD)
route.put('/pushd/:id',patientController.pushD)
route.post('/reft/:id',validate.isLoggedIn,patientController.refreshToken)

route.get('/getallpatient', function (req, res, next) {
    const permission = ac.can(req.query.role).readAny('get');
    console.log(permission)
    if (permission.granted) {
        patientSchema.find({}, function (err, data) {
           if (err || !data) return res.status(404).end();
            res.json({data});
       });
    } else {
        res.status(403).end();
    }
});

route.get('/getp',patientController.getPatient)




module.exports=route