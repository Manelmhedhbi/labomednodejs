const secController = require('../controllers/secretController')
const express= require('express')
const route = express.Router()//router gere routage
const file = require('../middelwaire/file')
//const jwt = require('jsonwebtoken')


route.post('/add',file.single('image'),secController.add)   
route.get('/getall',secController.getAll)
route.get('/getbyid/:id',secController.getbyid)
route.put('/update/:id',file.single('image'),secController.update)
route.delete('/delete/:id',secController.delete)
route.post('/authe',secController.authentification)
route.post('/logout',secController.logOut)
route.put('/pull/:id',secController.pullFunc)
route.put('/push/:id',secController.pushFunc)
module.exports=route