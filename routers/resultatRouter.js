const resController= require('../controllers/resultatController')
const express= require('express')
const route = express.Router()//router gere routage


route.post('/add',resController.add)   
route.get('/getall',resController.getAll)
route.get('/getbyid/:id',resController.getbyid)
route.put('/update/:id',resController.update)
route.delete('/delete/:id',resController.delete)

module.exports=route
