const chatController= require('../controllers/chatController')
const express= require('express')
const route = express.Router()



route.post('/add',chatController.add)   
route.get('/getall',chatController.getAll)
route.delete('/delete/:id',chatController.delete)



module.exports=route
