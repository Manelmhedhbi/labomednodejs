const consController= require('../controllers/consuController')
const express= require('express')
const route = express.Router()//router gere routage


route.post('/add',consController.add)   
route.get('/getall',consController.getAll)
route.get('/getbyid/:id',consController.getbyid)
route.put('/update/:id',consController.update)
route.delete('/delete/:id',consController.delete)

module.exports=route
