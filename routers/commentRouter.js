const commentController=require('../controllers/commentController')
const express= require('express')
const route = express.Router()

route.post('/add',commentController.addComment)
route.put('/update/:id',commentController.updateComment)
route.delete('/delete/:id',commentController.deleteComment)






module.exports=route