
const analyseController= require('../controllers/analyseController')
const express= require('express')
const route = express.Router()


route.post('/add',analyseController.add)   
route.get('/getall',analyseController.getAll)
route.get('/getbyid/:id',analyseController.getbyid)
route.put('/update/:id',analyseController.update)
route.delete('/delete/:id',analyseController.delete)

module.exports=route
