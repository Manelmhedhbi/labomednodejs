const AccessControl =require('accesscontrol');
const grantObjects = {
    medecin: {
            post: {
                'read:any': ['*'],
                'update:any': ['*'],
                'delete:any': ['*']
            },
            get: {
                'read:any': ['*'],
                'update:any': ['*'],
                'delete:any': ['*']
            }
        },
        user: {
           post: {
               'create:own': ['*'],
               'read:any': ['*'],
               'update:own': ['*'],
                'delete:own': ['*']
           }
        }
       
    }
const ac = new AccessControl(grantObjects);
module.exports= {ac}