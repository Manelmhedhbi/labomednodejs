const express = require('express')
var bodyParser = require("body-parser")
var cors = require('cors')
const data= require('./config/database')
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
const chatModel=require('./models/chatModel')

const medecinModel=require('./models/medecinModel')

const userRouter= require('./routers/userRouter')
const patientRouter= require('./routers/patientRouter')
const medecinRouter= require('./routers/medecinRouter')
const dossierRouter= require('./routers/dossierRouter')
const laboRouter= require('./routers/laboRouter')
const secRouter=require('./routers/secretaireRouter')
const consRouter=require('./routers/consuRouter')
const rdvRouter=require('./routers/rdvRouter')
const bilanRouter=require('./routers/bilanRouter')
const analyseRouter=require('./routers/analyseRouter')
const ordRouter=require('./routers/ordRouter')
const chatRouter=require('./routers/chatRouter')
const postRouter=require('./routers/postRouter')
const commentRouter=require('./routers/commentRouter')


const app = express()
app.set("secretkey","labomed")






app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/user',userRouter)
app.use('/patient',patientRouter)
app.use('/medecin',medecinRouter)
app.use('/dossier',dossierRouter)
app.use('/labo',laboRouter)
app.use('/sec',secRouter)
app.use('/con',consRouter)
app.use('/rdv',rdvRouter)
app.use('/bilan',bilanRouter)
app.use('/analyse',analyseRouter)
app.use('/ord',ordRouter)
app.use('/chat',chatRouter)
app.use('/post',postRouter)
app.use('/comment',commentRouter)

app.get('/getfile/:image',function(req,res){
    console.log(req.params.image)
    res.sendFile(__dirname+'/uploads/'+req.params.image)
   
   })

 // express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function(req, res, next) {
    let err = new Error;
       err.status = 404;
       next(err);
   });
// handle errors
app.use(function(err, req, res, next) {

    console.log(err);
    
     if(err.status === 404)
      res.status(404).json({message: "Path Not found"});
     else 
       res.status(500).json({message: "Something looks wrong :( !!!"});
   });
  

var http=app.listen(3500) 

const io = require('socket.io')(http, {
  cors: {
    origin: "http://localhost:4200",
    methods: ["GET", "POST"],
    allowedHeaders: ["my-custom-header"],
    credentials: true
  }
});
app.get('/', (req, res) => res.send('hello!'));
io.on('connection', (socket) => {  
 console.log('a user connected'); 
 socket.on('message', (msg) => {
  console.log(msg);
  socket.broadcast.emit('message-broadcast', msg);
   const dateNow= new Date()
    let  chatMessage  =  new chatModel({ message: msg.message, date:dateNow,sender: msg.iduser});
    chatMessage.save();
    });

});
console.log("server is running");
